import React, { useState } from "react"
import { Game } from "./components/Game";
import { Intro } from "./components/Intro";
import './styles/main.css'

export default function App() {
    const [show, setShow] = useState(false);
    const [seconds, setSeconds] = useState(30);

    if (show) {
        return <Game 
                setShow={setShow}
                seconds={seconds}
                setSeconds={setSeconds}
                />
    }
    return <Intro setSeconds={setSeconds} setShow={setShow}/>
}