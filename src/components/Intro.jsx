import { useState } from "react";

export function Intro(props) {
    const [temp, setTemp] = useState(30);

    return (
        <div className="intro">
            <h1>Welcome to Aim Test</h1>
            <p>Try to hit as many targets in the time limit</p>
            <p><strong>Good Luck</strong></p>
            <label>Enter time limit: 
                <input type="number" value={temp} onChange={(e) => setTemp(e.target.value)}/>
            </label>
            <br></br>
            <button className="next-btn" onClick={() => {
                props.setSeconds(temp);
                props.setShow(true);
            }}>Start Game</button>
        </div>
    );
}
