import { useEffect, useState } from "react";
import { Header } from "./Header";
import { Target } from "./Target";

export function GameOver(props) {
    return (
        <div className="intro">
            <h1>Finish!</h1>
            <p>You hit {props.count} targets in the span of 30 seconds!</p>
            <button onClick={() => props.setShow(false)}>Try again</button>
        </div>
    );
}

export function Game(props) {
    const [count, setCount] = useState(0);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (props.seconds > 0) {
                props.setSeconds(props.seconds - 1);
            } else {
                clearTimeout(timer);
            }
        }, 1000);
    });
    if (props.seconds !== 0) {
        return (
            <div className="game">
                <Header count={count} seconds={props.seconds} />
                <div className="targets">
                    <Target count={count} setCount={setCount} />
                </div>
            </div>
        );
    }
    return (
        <GameOver setShow={props.setShow} count={count}/>
    );
}