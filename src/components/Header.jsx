export function Header(props) {
    return (
        <header>
            <span>Targets hit: {props.count} <br></br> Seconds left: {props.seconds}</span>
        </header>
    );
}