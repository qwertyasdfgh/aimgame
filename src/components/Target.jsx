import { useState } from "react";
import { randomNumber } from "../utils/randomNumber";

export function Target(props) {
    const [style, setStyle] = useState({
        position: 'absolute',
        top: '50%',
        right: '50%'
    });

    return (
        <button onClick={() => {
            props.setCount(props.count + 1)
            setStyle({
                ...style,
                top: `${randomNumber()}%`,
                right: `${randomNumber()}%`
            })
        }} style={style} className="target">PRESS</button>
    );
}